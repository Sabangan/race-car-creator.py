def addCarStats():
    Stat = input("Which Stat? \n1.)Top Speed? \n2.)Acceleration? \n3.)Grip? \n4.)Brake? >  ")
    if Stat == "1":
        print(my_Car[1])
        amount = int(input("By how much > "))
        if (amount > my_Car[0]['points']) or (my_Car[0]['points'] <= 0):
            print("Not Enough Points")
        else:
            my_Car[1]['Top Speed'] += amount
            my_Car[0]['points'] -= amount
    elif Stat == "2":
        print(my_Car[2])
        amount = int(input("By how much > "))
        if (amount > my_Car[0]['points']) or (my_Car[0]['points'] <= 0):
            print("Not Enough Points")
        else:
            my_Car[2]['Acceleration'] += amount
            my_Car[0]['points'] -= amount
    elif Stat == "3":
        print(my_Car[3])
        amount = int(input("By how much > "))
        if (amount > my_Car[0]['points']) or (my_Car[0]['points'] <= 0):
            print("Not Enough Points")
        else:
            my_Car[3]['Grip'] += amount
            my_Car[0]['points'] -= amount
    elif Stat == "4":
        print(my_Car[4])
        amount = int(input("By how much > "))
        if (amount > my_Car[0]['points']) or (my_Car[0]['points'] <= 0):
            print("Not Enough Points")
        else:
            my_Car[4]['Brake'] += amount
            my_Car[0]['points'] -= amount
    else:
        print(Stat," does not exist")
            
def removeCarStats():
    Stat = input("Which Stat? \n1.)Top Speed? \n2.)Acceleration? \n3.)Grip? \n4.)Brake? >  ")
    if Stat == "1":
        print(my_Car[1])
        amount = int(input("By how much > "))
        if (amount > my_Car[1]['Top Speed']) or (my_Car[1]['Top Speed'] <= 0):
            print("Not Enough Points")
        else:
            my_Car[1]['Top Speed'] -= amount
            my_Car[0]['points'] += amount
    elif Stat == "2":
        print(my_Car[2])
        amount = int(input("By how much > "))
        if (amount > my_Car[2]['Acceleration']) or (my_Car[2]['Acceleration'] <= 0):
            print("Not Enough Points")
        else:
            my_Car[2]['Acceleration'] -= amount
            my_Car[0]['points'] += amount
    elif Stat == "3":
        print(my_Car[1])
        amount = int(input("By how much > "))
        if (amount > my_Car[3]['Grip']) or (my_Car[3]['Grip'] <= 0):
            print("Not Enough Points")
        else:
            my_Car[3]['Grip'] -= amount
            my_Car[0]['points'] += amount
    elif Stat == "4":
        print(my_Car[4])
        amount = int(input("By how much > "))
        if (amount > my_Car[4]['Brake']) or (my_Car[4]['Brake'] <= 0):
            print("Not Enough Points")
        else:
            my_Car[4]['Brake'] -= amount
            my_Car[0]['points'] += amount
    else:
        print(Stat," does not exist")
        
        
def printCar():
    print(name, " Stats : \n")
    for x in my_Car:
        print(x)
    print()
        
my_Car = [{'points':20},{"Top Speed":0},{"Acceleration":0},{"Grip":0},{"Brake":0}]
running = True
print("Create a Race Car! You have points to Top_Speed, Acceleration, Grip, and Brake.")

name = input("What is your Car's Name > ")

while running:
    print("You have ",my_Car[0]['points']," left.")
    print("1.) Add Points \n2.) Remove Points \n3.) See Stat \n4.) Exit\n")    
    choice = input("Choice > ")
    if choice == "1":
        addCarStats()
    elif choice == "2":
        removeCarStats()
    elif choice == "3":
        printCar()
    elif choice == "4":
        running = False
    else:
        print(choice, " is not a valid selection.\nTry again.")